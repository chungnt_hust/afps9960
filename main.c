/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "i2c_user.h"
#include "APDS9960.h"
#include "sysTickIsr.h"
#include "debug_sw_uart.h"

void sysTickISR()
{
    g_SysTime++;
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    SYSTICK_regSysTickISRCallback(sysTickISR);
    APDS9960_Init(10, (apds9960AGain_t)DEFAULT_PGAIN);
    
    for(;;)
    {
        /* Place your application code here. */
        uint8_t gesture = readGesture();
        if(gesture == APDS9960_DOWN) LOG_INFO("v");
        if(gesture == APDS9960_UP) LOG_INFO("^");
        if(gesture == APDS9960_LEFT) LOG_INFO("<");
        if(gesture == APDS9960_RIGHT) LOG_INFO(">");
    }
}

/* [] END OF FILE */
