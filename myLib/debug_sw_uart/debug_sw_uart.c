/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "debug_sw_uart.h"
#include <stdarg.h>
#include <stdio.h>
/* ---- DEBUG ----- */

//static uint8_t stringDebug[100];
//void DEBUG_UC(const char *format, ...)
//{
//	va_list arg;
//	va_start(arg, format);
//	vsprintf((char*)stringDebug, format, arg); 
//	va_end(arg);
//}

void DEBUG_SW_UART(const char *format, ...)
{
    const char8 string[50];
   	va_list arg;
	va_start(arg, format);
	vsprintf((char*)string, format, arg); 
    DEBUG_PutString(string);
	va_end(arg); 
}

/* --------------- */
/* [] END OF FILE */
