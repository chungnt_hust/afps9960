/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "APDS9960.h"
#include "i2c_user.h"
#include "debug_sw_uart.h"
#include "sysTickIsr.h"
#include <stdlib.h>

static enable_t enableReg;
static control_t controlReg;
static gconf3_t gconf3Reg;
static gconf1_t gconf1Reg;
static gconf2_t gconf2Reg;
static gpulse_t gpulseReg;
static gstatus_t gstatusReg;

static uint8_t gestCnt;
static uint8_t UCount;
static uint8_t DCount;
static uint8_t LCount;
static uint8_t RCount;

static bool checkDeviceID(void)
{
    uint8_t id;
    i2c_user_master_readByte(APDS9960_ID, &id);
    if(id != APDS9960_DEVICE_ID) return false;
    return true;
}

// power on
static void powerOn(bool en)
{
    enableReg.bitFeild.PON = en;
    i2c_user_master_writeByte(APDS9960_ENABLE, enableReg.byteData);
}
// proximity
static void enableProximity(bool en)
{
    enableReg.bitFeild.PEN = en;
    i2c_user_master_writeByte(APDS9960_ENABLE, enableReg.byteData);
}
// gesture
static void enableGesture(bool en)
{
    enableReg.bitFeild.GEN = en;
    i2c_user_master_writeByte(APDS9960_ENABLE, enableReg.byteData);
}

static void setADCIntegrationTime(uint16_t iTimeMS) 
{
  float temp;
  // convert ms into 2.78ms increments
  temp = iTimeMS;
  temp /= 2.78;
  temp = 256 - temp;
  if (temp > 255)
    temp = 255;
  if (temp < 0)
    temp = 0;

  /* Update the timing register */
  i2c_user_master_writeByte(APDS9960_ATIME, (uint8_t)temp);
}

static void setADCGain(apds9960AGain_t aGain) 
{
  controlReg.bitFeild.AGAIN = aGain;
  /* Update the timing register */
  i2c_user_master_writeByte(APDS9960_CONTROL, controlReg.byteData);
}

static void setGestureDimensions(uint8_t dims) 
{
  gconf3Reg.bitFeild.GDIMS = dims;
  i2c_user_master_writeByte(APDS9960_GCONF3, gconf3Reg.byteData);
}

static void setGestureFIFOThreshold(uint8_t thresh) 
{
  gconf1Reg.bitFeild.GFIFOTH = thresh;
  i2c_user_master_writeByte(APDS9960_GCONF1, gconf1Reg.byteData);
}

static void setGestureGain(uint8_t gain) 
{
  gconf2Reg.bitFeild.GGAIN = gain;
  i2c_user_master_writeByte(APDS9960_GCONF2, gconf2Reg.byteData);
}

static void setGestureProximityThreshold(uint8_t thresh) 
{
  i2c_user_master_writeByte(APDS9960_GPENTH, thresh);
}

static void resetCounts() 
{
  gestCnt = 0;
  UCount = 0;
  DCount = 0;
  LCount = 0;
  RCount = 0;
}

void APDS9960_Init(uint16_t iTimeMS, apds9960AGain_t aGain)
{
    CyDelay(10); // initialize: 5.7ms, then device go to the sleep mode
    i2c_user_master_init(APDS9960_ADDRESS);
        
    if(checkDeviceID() == true) LOG_INFO("Read Device ID ok\n");
    else LOG_INFO("Read Device ID false\n");
   
    /* Set default integration time and gain */
    setADCIntegrationTime(iTimeMS);
    setADCGain(aGain);
//    powerOn(true);          // Power ON bit
    /* exit sleep mode: PEN || GEN || AEN == 1 */
    enableProximity(false);
    enableGesture(false);
//    CyDelay(10);            // EXIT SLEEP 7ms
    /*                      */  
    powerOn(false);          // Power ON bit
    CyDelay(10);
    powerOn(true);          // Power ON bit
    CyDelay(10);

    
    // default to all gesture dimensions
    setGestureDimensions(APDS9960_DIMENSIONS_ALL);
    setGestureFIFOThreshold(APDS9960_GFIFO_4);
    setGestureGain(APDS9960_GGAIN_4);
    setGestureProximityThreshold(50);
    resetCounts();
    
    gpulseReg.bitFeild.GPLEN = APDS9960_GPULSE_32US;
    gpulseReg.bitFeild.GPULSE = 9; // 10 pulses
    i2c_user_master_writeByte(APDS9960_GPULSE, gpulseReg.byteData);
    
    enableProximity(true);
    enableGesture(true);
}

static bool gestureValid() 
{
  i2c_user_master_readByte(APDS9960_GSTATUS, &gstatusReg.byteData);
  return gstatusReg.bitFeild.GVALID;
}

bool readGesture() 
{
  uint8_t toRead;
  uint8_t buf[256];
  uint32_t t = 0;
  uint8_t gestureReceived;
  while (1) {
    int up_down_diff = 0;
    int left_right_diff = 0;
    gestureReceived = 0;
    if (!gestureValid())
      return 0;

    CyDelay(30);
    i2c_user_master_readByte(APDS9960_GFLVL, &toRead);

    // bytesRead is unused but produces sideffects needed for readGesture to work
    i2c_user_master_readData(APDS9960_GFIFO_U, (uint8_t*)&buf, 4*toRead);

    if (abs((int)buf[0] - (int)buf[1]) > 13)
      up_down_diff += (int)buf[0] - (int)buf[1];

    if (abs((int)buf[2] - (int)buf[3]) > 13)
      left_right_diff += (int)buf[2] - (int)buf[3];

    if (up_down_diff != 0) {
      if (up_down_diff < 0) {
        if (DCount > 0) {
          gestureReceived = APDS9960_UP;
        } else
          UCount++;
      } else if (up_down_diff > 0) {
        if (UCount > 0) {
          gestureReceived = APDS9960_DOWN;
        } else
          DCount++;
      }
    }

    if (left_right_diff != 0) {
      if (left_right_diff < 0) {
        if (RCount > 0) {
          gestureReceived = APDS9960_LEFT;
        } else
          LCount++;
      } else if (left_right_diff > 0) {
        if (LCount > 0) {
          gestureReceived = APDS9960_RIGHT;
        } else
          RCount++;
      }
    }

    if (up_down_diff != 0 || left_right_diff != 0)
      t = g_SysTime;

    if (gestureReceived || g_SysTime - t > 300) {
      resetCounts();
      return gestureReceived;
    }
  }
}
/* [] END OF FILE */
