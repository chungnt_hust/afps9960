/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef _APDS9960_H
#define _APDS9960_H
#include <stdbool.h>
#include "project.h"
    
#define APDS9960_ADDRESS   (0x39) /**< I2C Address */
#define APDS9960_DEVICE_ID (0xAB)
    
/** I2C Registers */
enum {
  APDS9960_RAM = 0x00,
  APDS9960_ENABLE = 0x80,
  APDS9960_ATIME = 0x81,
  APDS9960_WTIME = 0x83,
  APDS9960_AILTIL = 0x84,
  APDS9960_AILTH = 0x85,
  APDS9960_AIHTL = 0x86,
  APDS9960_AIHTH = 0x87,
  APDS9960_PILT = 0x89,
  APDS9960_PIHT = 0x8B,
  APDS9960_PERS = 0x8C,
  APDS9960_CONFIG1 = 0x8D,
  APDS9960_PPULSE = 0x8E,
  APDS9960_CONTROL = 0x8F,
  APDS9960_CONFIG2 = 0x90,
  APDS9960_ID = 0x92,
  APDS9960_STATUS = 0x93,
  APDS9960_CDATAL = 0x94,
  APDS9960_CDATAH = 0x95,
  APDS9960_RDATAL = 0x96,
  APDS9960_RDATAH = 0x97,
  APDS9960_GDATAL = 0x98,
  APDS9960_GDATAH = 0x99,
  APDS9960_BDATAL = 0x9A,
  APDS9960_BDATAH = 0x9B,
  APDS9960_PDATA = 0x9C,
  APDS9960_POFFSET_UR = 0x9D,
  APDS9960_POFFSET_DL = 0x9E,
  APDS9960_CONFIG3 = 0x9F,
  APDS9960_GPENTH = 0xA0,
  APDS9960_GEXTH = 0xA1,
  APDS9960_GCONF1 = 0xA2,
  APDS9960_GCONF2 = 0xA3,
  APDS9960_GOFFSET_U = 0xA4,
  APDS9960_GOFFSET_D = 0xA5,
  APDS9960_GOFFSET_L = 0xA7,
  APDS9960_GOFFSET_R = 0xA9,
  APDS9960_GPULSE = 0xA6,
  APDS9960_GCONF3 = 0xAA,
  APDS9960_GCONF4 = 0xAB,
  APDS9960_GFLVL = 0xAE,
  APDS9960_GSTATUS = 0xAF,
  APDS9960_IFORCE = 0xE4,
  APDS9960_PICLEAR = 0xE5,
  APDS9960_CICLEAR = 0xE6,
  APDS9960_AICLEAR = 0xE7,
  APDS9960_GFIFO_U = 0xFC,
  APDS9960_GFIFO_D = 0xFD,
  APDS9960_GFIFO_L = 0xFE,
  APDS9960_GFIFO_R = 0xFF,
};

/** ADC gain settings */
typedef enum {
  APDS9960_AGAIN_1X = 0x00,  /**< No gain */
  APDS9960_AGAIN_4X = 0x01,  /**< 2x gain */
  APDS9960_AGAIN_16X = 0x02, /**< 16x gain */
  APDS9960_AGAIN_64X = 0x03  /**< 64x gain */
} apds9960AGain_t;

/** Proxmity gain settings */
typedef enum {
  APDS9960_PGAIN_1X = 0x00, /**< 1x gain */
  APDS9960_PGAIN_2X = 0x04, /**< 2x gain */
  APDS9960_PGAIN_4X = 0x08, /**< 4x gain */
  APDS9960_PGAIN_8X = 0x0C  /**< 8x gain */
} apds9960PGain_t;

/** Pulse length settings */
typedef enum {
  APDS9960_PPULSELEN_4US = 0x00,  /**< 4uS */
  APDS9960_PPULSELEN_8US = 0x40,  /**< 8uS */
  APDS9960_PPULSELEN_16US = 0x80, /**< 16uS */
  APDS9960_PPULSELEN_32US = 0xC0  /**< 32uS */
} apds9960PPulseLen_t;

/** LED drive settings */
typedef enum {
  APDS9960_LEDDRIVE_100MA = 0x00, /**< 100mA */
  APDS9960_LEDDRIVE_50MA = 0x40,  /**< 50mA */
  APDS9960_LEDDRIVE_25MA = 0x80,  /**< 25mA */
  APDS9960_LEDDRIVE_12MA = 0xC0   /**< 12.5mA */
} apds9960LedDrive_t;

/** LED boost settings */
typedef enum {
  APDS9960_LEDBOOST_100PCNT = 0x00, /**< 100% */
  APDS9960_LEDBOOST_150PCNT = 0x10, /**< 150% */
  APDS9960_LEDBOOST_200PCNT = 0x20, /**< 200% */
  APDS9960_LEDBOOST_300PCNT = 0x30  /**< 300% */
} apds9960LedBoost_t;

/** Dimensions */
enum {
  APDS9960_DIMENSIONS_ALL = 0x00,        // All dimensions
  APDS9960_DIMENSIONS_UP_DOWN = 0x01,    // Up/Down dimensions
  APGS9960_DIMENSIONS_LEFT_RIGHT = 0x02, // Left/Right dimensions
};

/** FIFO Interrupts */
enum {
  APDS9960_GFIFO_1 = 0x00,  // Generate interrupt after 1 dataset in FIFO
  APDS9960_GFIFO_4 = 0x01,  // Generate interrupt after 2 datasets in FIFO
  APDS9960_GFIFO_8 = 0x02,  // Generate interrupt after 3 datasets in FIFO
  APDS9960_GFIFO_16 = 0x03, // Generate interrupt after 4 datasets in FIFO
};

/** Gesture Gain */
enum {
  APDS9960_GGAIN_1 = 0x00, // Gain 1x
  APDS9960_GGAIN_2 = 0x01, // Gain 2x
  APDS9960_GGAIN_4 = 0x02, // Gain 4x
  APDS9960_GGAIN_8 = 0x03, // Gain 8x
};

/** Pulse Lenghts */
enum {
  APDS9960_GPULSE_4US = 0x00,  // Pulse 4us
  APDS9960_GPULSE_8US = 0x01,  // Pulse 8us
  APDS9960_GPULSE_16US = 0x02, // Pulse 16us
  APDS9960_GPULSE_32US = 0x03, // Pulse 32us
};

/* Default values */
#define DEFAULT_ATIME           219     // 103ms
//#define DEFAULT_WTIME           246     // 27ms
//#define DEFAULT_PROX_PPULSE     0x87    // 16us, 8 pulses
//#define DEFAULT_GESTURE_PPULSE  0x89    // 16us, 10 pulses
//#define DEFAULT_POFFSET_UR      0       // 0 offset
//#define DEFAULT_POFFSET_DL      0       // 0 offset      
//#define DEFAULT_CONFIG1         0x60    // No 12x wait (WTIME) factor
//#define DEFAULT_LDRIVE          APDS9960_LEDDRIVE_100MA
#define DEFAULT_PGAIN           APDS9960_PGAIN_4X
//#define DEFAULT_AGAIN           APDS9960_AGAIN_4X
//#define DEFAULT_PILT            0       // Low proximity threshold
//#define DEFAULT_PIHT            50      // High proximity threshold
//#define DEFAULT_AILT            0xFFFF  // Force interrupt for calibration
//#define DEFAULT_AIHT            0
//#define DEFAULT_PERS            0x11    // 2 consecutive prox or ALS for int.
//#define DEFAULT_CONFIG2         0x01    // No saturation interrupts or LED boost  
//#define DEFAULT_CONFIG3         0       // Enable all photodiodes, no SAI
//#define DEFAULT_GPENTH          40      // Threshold for entering gesture mode
//#define DEFAULT_GEXTH           30      // Threshold for exiting gesture mode    
//#define DEFAULT_GCONF1          0x40    // 4 gesture events for int., 1 for exit
//#define DEFAULT_GGAIN           APDS9960_GGAIN_4
//#define DEFAULT_GLDRIVE         APDS9960_LEDDRIVE_100MA
//#define DEFAULT_GWTIME          APDS9960_GPULSE_8US
//#define DEFAULT_GOFFSET         0       // No offset scaling for gesture mode
//#define DEFAULT_GPULSE          0xC9    // 32us, 10 pulses
//#define DEFAULT_GCONF3          0       // All photodiodes active during gesture
//#define DEFAULT_GIEN            0       // Disable gesture interrupts

#define APDS9960_UP 0x01    /**< Gesture Up */
#define APDS9960_DOWN 0x02  /**< Gesture Down */
#define APDS9960_LEFT 0x03  /**< Gesture Left */
#define APDS9960_RIGHT 0x04 /**< Gesture Right */

// enable
typedef struct  
{
    // power on
    uint8_t PON : 1;
    // ALS enable
    uint8_t AEN : 1;
    // Proximity detect enable
    uint8_t PEN : 1;
    // wait timer enable
    uint8_t WEN : 1;
    // ALS interrupt enable
    uint8_t AIEN : 1;
    // proximity interrupt enable
    uint8_t PIEN : 1;
    // gesture enable
    uint8_t GEN : 1;
} enableStructure;

typedef union
{
    enableStructure bitFeild;
    uint8_t byteData;
} enable_t;

// control
typedef struct  
{
    // ALS and Color gain control
    uint8_t AGAIN : 2;
    // proximity gain control
    uint8_t PGAIN : 2;
    // led drive strength
    uint8_t LDRIVE : 2;
} controlStructer;

typedef union
{
    controlStructer bitFeild;
    uint8_t byteData;
} control_t;

// gconf3
typedef struct 
{
    /* Gesture Dimension Select. Selects which gesture photodiode pairs are
    enabled to gather results during gesture.
    */
    uint8_t GDIMS : 2;
} gconf3Structure;

typedef union
{
    gconf3Structure bitFeild;
    uint8_t byteData;
} gconf3_t;

// gconf1
typedef struct 
{
    /* Gesture Exit Persistence. When a number of consecutive gesture end
    occurrences become equal or greater to the GEPERS value, the Gesture state
    machine is exited.
    */
    uint8_t GEXPERS : 2;

    /* Gesture Exit Mask. Controls which of the gesture detector photodiodes
    (UDLR) will be included to determine a gesture end and subsequent exit
    of the gesture state machine. Unmasked UDLR data will be compared with the
    value in GTHR_OUT. Field value bits correspond to UDLR detectors.
    */
    uint8_t GEXMSK : 4;

    /* Gesture FIFO Threshold. This value is compared with the FIFO Level (i.e.
    the number of UDLR datasets) to generate an interrupt (if enabled).
    */
    uint8_t GFIFOTH : 2;
} gconf1Structure;

typedef union
{
    gconf1Structure bitFeild;
    uint8_t byteData;
} gconf1_t;

// gconf2
typedef struct 
{
    /* Gesture Wait Time. The GWTIME controls the amount of time in a low power
    mode between gesture detection cycles.
    */
    uint8_t GWTIME : 3;

    // Gesture LED Drive Strength. Sets LED Drive Strength in gesture mode.
    uint8_t GLDRIVE : 2;

    // Gesture Gain Control. Sets the gain of the proximity receiver in gesture
    // mode.
    uint8_t GGAIN : 2;
} gconf2Structure;

typedef union
{
    gconf2Structure bitFeild;
    uint8_t byteData;
} gconf2_t;

// gpulse
typedef struct 
{
    /* Number of Gesture Pulses. Specifies the number of pulses to be generated
    on LDR. Number of pulses is set by GPULSE value plus 1.
    */
    uint8_t GPULSE : 6;

    // Gesture Pulse Length. Sets the LED_ON pulse width during a Gesture LDR
    // Pulse.
    uint8_t GPLEN : 2;
} gpulseStructure;

typedef union
{
    gpulseStructure bitFeild;
    uint8_t byteData;
} gpulse_t;
  
// gstatus
typedef struct 
{
    /* Gesture FIFO Data. GVALID bit is sent when GFLVL becomes greater than
    GFIFOTH (i.e. FIFO has enough data to set GINT). GFIFOD is reset when GMODE
    = 0 and the GFLVL=0 (i.e. All FIFO data has been read).
    */
    uint8_t GVALID : 1;

    /* Gesture FIFO Overflow. A setting of 1 indicates that the FIFO has filled
    to capacity and that new gesture detector data has been lost.
    */
    uint8_t GFOV : 1;
} gstatusStructure;

typedef union
{
    gstatusStructure bitFeild;
    uint8_t byteData;
} gstatus_t;

void APDS9960_Init(uint16_t iTimeMS, apds9960AGain_t aGain);
bool readGesture() ;
#endif
/* [] END OF FILE */
