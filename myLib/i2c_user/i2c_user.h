/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef _I2C_USER_H
#define _I2C_USER_H
#include "I2CM_I2C.h"
#include "I2CM.h"
    
/* To import include header file:
--> Project
--> Build setting
--> ARM GCC 
--> Compiler
--> General
--> In General tab: import folder contain header file */
    
/*----------------------------------------*/
            /* MASTER MODE */
/*----------------------------------------*/    
typedef uint32_t I2CM_ErrorTypedef_t;
//#define I2CM_I2C_MSTR_NO_ERROR          (0x00u)  /* Function complete without error                       */
//#define I2CM_I2C_MSTR_ERR_ARB_LOST      (0x01u)  /* Master lost arbitration: INTR_MASTER_I2C_ARB_LOST     */
//#define I2CM_I2C_MSTR_ERR_LB_NAK        (0x02u)  /* Last Byte Naked: INTR_MASTER_I2C_NACK                 */
//#define I2CM_I2C_MSTR_NOT_READY         (0x04u)  /* Master on the bus or Slave operation is in progress   */
//#define I2CM_I2C_MSTR_BUS_BUSY          (0x08u)  /* Bus is busy, process not started                      */
//#define I2CM_I2C_MSTR_ERR_ABORT_START   (0x10u)  /* Slave was addressed before master begin Start gen     */
//#define I2CM_I2C_MSTR_ERR_BUS_ERR       (0x100u) /* Bus error has: INTR_MASTER_I2C_BUS_ERROR              */
//#define I2CM_I2C_MSTR_ERR_TIMEOUT       (I2CM_I2C_MASTER_TIMEOUT) /* Operation timeout  

void i2c_user_master_init(uint8_t slaveAdd);
I2CM_ErrorTypedef_t i2c_user_master_writeByte(uint8_t add, uint8_t byte);
I2CM_ErrorTypedef_t i2c_user_master_readByte(uint8_t add, uint8_t *byte);
I2CM_ErrorTypedef_t i2c_user_master_writeData(uint8_t add, void *dataTx, uint32_t sizeData);
I2CM_ErrorTypedef_t i2c_user_master_readData(uint8_t add, void *dataRx, uint32_t sizeData);
/*----------------------------------------*/
        /* END OF MASTER MODE */
/*----------------------------------------*/
#endif
/* [] END OF FILE */
