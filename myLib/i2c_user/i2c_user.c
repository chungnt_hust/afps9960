/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "i2c_user.h"
/*----------------------------------------*/
            /* MASTER MODE */
        /* Module name: I2CM */
/*----------------------------------------*/
static uint8_t i2c_slaveAdd;

/******************************************
* init slave address in master mode
******************************************/
void i2c_user_master_init(uint8_t slaveAdd)
{
    i2c_slaveAdd = slaveAdd;
    I2CM_Start();
}

/******************************************
* master write 1 byte to slaver at address
* return error type
* example: i2c_user_master_writeByte(0x00, dataIn);
******************************************/
I2CM_ErrorTypedef_t i2c_user_master_writeByte(uint8_t add, uint8_t byte)
{
    I2CM_ErrorTypedef_t status = I2CM_I2C_MSTR_NO_ERROR;
    status = I2CM_I2CMasterSendStart(i2c_slaveAdd, I2CM_I2C_WRITE_XFER_MODE, 100);
    status = I2CM_I2CMasterWriteByte(add, 100);
    status = I2CM_I2CMasterWriteByte(byte, 100);
    status = I2CM_I2CMasterSendStop(100);
    return status;
}

/******************************************
* master read 1 byte from slaver at address
* return error type
* example: i2c_user_master_readByte(0x00, &dataOut);
******************************************/
I2CM_ErrorTypedef_t i2c_user_master_readByte(uint8_t add, uint8_t *byte)
{
    I2CM_ErrorTypedef_t status = I2CM_I2C_MSTR_NO_ERROR;
    status = I2CM_I2CMasterSendStart(i2c_slaveAdd, I2CM_I2C_WRITE_XFER_MODE, 100);
    status = I2CM_I2CMasterWriteByte(add, 100);
    status = I2CM_I2CMasterSendRestart(i2c_slaveAdd, I2CM_I2C_READ_XFER_MODE, 100);
    status = I2CM_I2CMasterReadByte(I2CM_I2C_NAK_DATA, byte, 100);
    status = I2CM_I2CMasterSendStop(100);
    return status;
}

/******************************************
* master write data typedef to slaver at address
* return error type
* example: 
*         - dataTypedef_t dataIn;
*         - i2c_user_master_writeData(0x00, (dataTypedef_t)&dataIn, sizeof(dataIn));
******************************************/
I2CM_ErrorTypedef_t i2c_user_master_writeData(uint8_t add, void *dataTx, uint32_t sizeData)
{
    I2CM_ErrorTypedef_t status = I2CM_I2C_MSTR_NO_ERROR;
    uint32_t len;
    uint8_t *p;
    p = (uint8_t*)dataTx;
    status = I2CM_I2CMasterSendStart(i2c_slaveAdd, I2CM_I2C_WRITE_XFER_MODE, 100);
    status = I2CM_I2CMasterWriteByte(add, 100);
    for(len = 0; len < sizeData; len++)
    {
        status = I2CM_I2CMasterWriteByte(p[len], 100);
    }
    status = I2CM_I2CMasterSendStop(100);
    return status;
}

/******************************************
* master read data typedef from slaver at address
* return error type
* example: 
*         - dataTypedef_t dataOut;
*         - i2c_user_master_readData(0x00, (dataTypedef_t)&dataOut, sizeof(dataOut));
******************************************/
I2CM_ErrorTypedef_t i2c_user_master_readData(uint8_t add, void *dataRx, uint32_t sizeData)
{
    I2CM_ErrorTypedef_t status = I2CM_I2C_MSTR_NO_ERROR;
    uint32_t len;
    uint8_t *p;
    p = (uint8_t*)dataRx;
    status = I2CM_I2CMasterSendStart(i2c_slaveAdd, I2CM_I2C_WRITE_XFER_MODE, 100);
    status = I2CM_I2CMasterWriteByte(add, 100);
    status = I2CM_I2CMasterSendRestart(i2c_slaveAdd, I2CM_I2C_READ_XFER_MODE, 100);
    for(len = 0; len < sizeData-1; len++)
    {
        status = I2CM_I2CMasterReadByte(I2CM_I2C_ACK_DATA, &p[len], 100);
    }
    status = I2CM_I2CMasterReadByte(I2CM_I2C_NAK_DATA, &p[len], 100);
    status = I2CM_I2CMasterSendStop(100);
    return status;
}
/*----------------------------------------*/
        /* END OF MASTER MODE */
/*----------------------------------------*/
/* [] END OF FILE */
